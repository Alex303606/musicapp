import {
	ADD_COCKTAIL,
	CHANGE_COCKTAIL_RATE,
	FETCH_COCKTAIL,
	FETCH_COCKTAIL_BY_ID,
	GOTO_COCKTAIL_PAGE
} from "./actionType";

export const addCocktail = cocktailData => {
	return {type: ADD_COCKTAIL, cocktailData};
};

export const fetchCocktail = () => {
	return {type: FETCH_COCKTAIL};
};

export const goToCocktailPage = id => {
	return {type: GOTO_COCKTAIL_PAGE, id};
};

export const fetchCocktailById = id => {
	return {type: FETCH_COCKTAIL_BY_ID, id};
};

export const submitCocktailRating = (id, rate) => {
	return {type: CHANGE_COCKTAIL_RATE, id, rate};
};