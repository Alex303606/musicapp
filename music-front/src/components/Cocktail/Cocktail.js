import React from 'react';
import notFound from '../../assets/images/not-found.jpeg';
import './Cocktail.css';
import config from "../../config";

const Cocktail = ({cocktailImage, name , click}) => {
	
	let image = `url(${notFound})`;
	
	if (cocktailImage) image = `url(${config.apiUrl}uploads/${cocktailImage})`;
	return (
		<div className="cocktails_item" onClick={click}>
			<div className="image" style={{
				backgroundImage: image,
				backgroundRepeat: 'no-repeat',
				backgroundSize: 'cover',
				backgroundPosition: 'center'
			}}/>
			<div className="name">{name}</div>
		</div>
	);
};

export default Cocktail;
