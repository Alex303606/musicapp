import { FETCH_ALBUM_TRACKS_SUCCESS } from "../actions/actionType";

const initialState = {
	currentAlbumTracks: []
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case FETCH_ALBUM_TRACKS_SUCCESS:
			return {...state, currentAlbumTracks: action.tracks};
		default:
			return state;
	}
};

export default reducer;