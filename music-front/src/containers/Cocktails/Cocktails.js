import React, { Component } from 'react';
import { connect } from 'react-redux';
import './Cocktails.css';
import { NavLink, Route, Switch } from "react-router-dom";
import CocktailsList from "../../components/CocktailsList/CocktailsList";
import AddCocktail from "../../components/AddCocktail/AddCocktail";

class Cocktails extends Component {
	render() {
		return (
			<div className="container cocktails">
				<header className="cocktails_header">
					<ul>
						<li><h1>Cocktails</h1></li>
						<li><NavLink to="/cocktails">Cocktails</NavLink></li>
						<li><NavLink to="/cocktails/add_cocktail"><span>Add new cocktail</span></NavLink></li>
					</ul>
				</header>
				<Switch>
					<Route
						render={() => <CocktailsList/>}
						path="/cocktails" exact/>
					<Route
						render={() => <AddCocktail/>}
						path="/cocktails/add_cocktail" exact/>
				</Switch>
			</div>
		);
	}
}

const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(Cocktails);
