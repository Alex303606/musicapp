import React, { Component } from 'react';
import { connect } from 'react-redux';
import './TracksHistory.css';
import { getTrackHistory } from "../../store/actions/tracks_history";
import { toggleModalLogin } from "../../store/actions/users";
import HistoryItem from "../../components/HistoryItem/HistoryItem";

class TracksHistory extends Component {
	
	componentDidMount() {
		if(this.props.user) {
			this.props.getTrackHistory();
		} else {
			this.props.history.push('/');
			this.props.toggleModalLogin();
		}
	}
	
	render() {
		return (
			<div className="tracks_history container">
				<h1>Tracks history</h1>
				<ul>
					{
						this.props.tracks_history.map(item => {
							return <HistoryItem
								key={item._id}
								artist={item.track.album.artist.name}
								track={item.track.name}
								date={item.dateTime}
							
							/>;
						})
					}
				</ul>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	tracks_history: state.his.tracks_history,
	user: state.usr.user
});
const mapDispatchToProps = dispatch => ({
	getTrackHistory: () => dispatch(getTrackHistory()),
	toggleModalLogin: () => dispatch(toggleModalLogin())
});

export default connect(mapStateToProps,mapDispatchToProps)(TracksHistory);
