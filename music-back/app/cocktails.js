const express = require('express');
const multer = require('multer');
const nanoid = require('nanoid');
const path = require('path');
const Cocktails = require('../models/Cocktails');
const auth = require('../middleware/auth');
const config = require('../config');

const storage = multer.diskStorage({
	destination: (req, file, cb) => {
		cb(null, config.uploadPath);
	},
	filename: (req, file, cb) => {
		cb(null, nanoid() + path.extname(file.originalname));
	}
});

const upload = multer({storage});

const router = express.Router();

const createRouter = () => {
	
	router.post('/', [auth, upload.single('image')], (req, res) => {
		const cocktailsData = req.body;
		cocktailsData.ingredients = JSON.parse(cocktailsData.ingredients);
		cocktailsData.user = req.user._id;
		if (req.file) {
			cocktailsData.image = req.file.filename;
		} else {
			cocktailsData.image = null;
		}
		
		const cocktail = new Cocktails(cocktailsData);
		
		cocktail.save()
		.then(result => res.send(result))
		.catch(error => res.status(400).send(error));
	});
	
	router.get('/', (req, res) => {
		Cocktails.find({public: true})
		.then(result => {
			if (result) res.send(result);
			else res.sendStatus(404);
		})
		.catch(() => res.sendStatus(500));
	});
	
	router.get('/:id', (req, res) => {
		Cocktails.findOne({_id: req.params.id})
		.then(result => {
			if (result) res.send(result);
			else res.sendStatus(404);
		})
		.catch(() => res.sendStatus(500));
	});
	
	router.post('/rate', auth, (req, res) => {
		Cocktails.findOne({_id: req.body.id}, (err, cocktail) => {
			const index = cocktail.votes.findIndex(vote => vote.rateUser.equals(req.user._id));
			if (index === -1) {
				cocktail.votes = [...cocktail.votes, {rate: req.body.rate, rateUser: req.user._id}];
			}
			else {
				cocktail.votes[index].rate = req.body.rate;
			}
			const result = cocktail.votes.reduce((sum, vote) => {
				return sum + vote.rate;
			}, 0);
			cocktail.rating = result / cocktail.votes.length;
			cocktail.save(function (err) {
				if (err) console.error('ERROR!', err);
			});
		}).then(result => {
			if (result) res.send(result);
			else res.sendStatus(404);
		})
		.catch(() => res.sendStatus(500));
	});
	
	return router;
};

module.exports = createRouter;