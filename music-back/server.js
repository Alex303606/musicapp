const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const config = require('./config');
const users = require('./app/users');
const trackHistory = require('./app/trackHistory');
const artists = require('./app/artists');
const albums = require('./app/albums');
const tracks = require('./app/tracks');
const admin = require('./app/admin');
const chat = require('./app/chat');
const cocktails = require('./app/cocktails');
const app = express();
require('express-ws')(app);

app.use(cors());
app.use(express.json());
app.use(express.static('public'));

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;
const port = 8000;

db.once('open', () => {
	console.log('Mongoose connected!');
	app.use('/users', users());
	app.use('/track_history', trackHistory());
	app.use('/artists', artists());
	app.use('/albums', albums());
	app.use('/tracks', tracks());
	app.use('/admin', admin());
	app.use('/chat', chat());
	app.use('/cocktails', cocktails());
	
	app.listen(8000, () => {
		console.log(`Server started on ${port} port!`);
	});
});
