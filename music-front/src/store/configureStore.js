import { applyMiddleware, combineReducers, compose, createStore } from "redux";
import { routerMiddleware, routerReducer } from "react-router-redux";
import createHistory from "history/createBrowserHistory";
import thunkMiddleware from "redux-thunk";
import createSagaMiddleware from 'redux-saga';
import { loadState, saveState } from "./localStorage";
import artistsReducer from "./reducers/artists";
import historyReducer from "./reducers/tracks_history";
import tracksReducer from "./reducers/tracks";
import albumsReducer from "./reducers/albums";
import userReducer from "./reducers/users";
import adminReducer from "./reducers/admin";
import messagesReducer from "./reducers/messages";
import cocktailsReducer from "./reducers/coctails";
import { watchCocktails } from './sagas';

const rootReducer = combineReducers({
	art: artistsReducer,
	alb: albumsReducer,
	trc: tracksReducer,
	his: historyReducer,
	usr: userReducer,
	admin: adminReducer,
	messages: messagesReducer,
	cocktails: cocktailsReducer,
	routing: routerReducer
});

export const history = createHistory();

const sagaMiddleware = createSagaMiddleware();

const middleware = [
	thunkMiddleware,
	routerMiddleware(history),
	sagaMiddleware
];

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const enhancers = composeEnhancers(applyMiddleware(...middleware));

const persistedState = loadState();

const store = createStore(rootReducer, persistedState, enhancers);

sagaMiddleware.run(watchCocktails);

store.subscribe(() => {
	saveState({
		usr: store.getState().usr
	});
});

export default store;