import React, { Component } from 'react';
import { connect } from 'react-redux';
import './Register.css';
import { registerUser } from "../../store/actions/users";

class Register extends Component {
	
	state = {
		username: '',
		password: ''
	};
	
	inputChangeHandler = event => {
		this.setState({
			[event.target.name]: event.target.value
		});
	};
	
	submitFormHandler = event => {
		event.preventDefault();
		
		this.props.registerUser(this.state);
	};
	
	render() {
		return (
			<div className="register_user container">
				<h1>Register new user</h1>
				<form onSubmit={this.submitFormHandler}>
					<div className="row">
						<label htmlFor="username">Enter username:</label>
						<input value={this.state.username} onChange={this.inputChangeHandler} id="username" name="username" type="text"/>
					</div>
					<div className="row">
						<label htmlFor="password">Enter password:</label>
						<input value={this.state.password} onChange={this.inputChangeHandler} id="password" name="password" type="password"/>
					</div>
					<div className="row">
						<button><span>Register</span></button>
					</div>
				</form>
			</div>
		);
	}
}

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({
	registerUser: userData => dispatch(registerUser(userData))
});

export default connect(mapStateToProps, mapDispatchToProps)(Register);
