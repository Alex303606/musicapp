const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const Users = require('../models/Users');

const MessagesSchema = new Schema({
	user: {
		type: Schema.Types.ObjectId,
		required: true,
		ref: 'Users'
	},
	text: {
		type: String,
		required: true
	},
	dateTime: {
		type: Date,
		default: Date.now
	}
});

const Messages = mongoose.model('Messages', MessagesSchema);

module.exports = Messages;
