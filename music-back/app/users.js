const express = require('express');
const Users = require('../models/Users');
const auth = require('../middleware/auth');
const router = express.Router();
const config = require('../config');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const https = require('https');
const request = require('request-promise-native');
const storage = multer.diskStorage({
	destination: (req, file, cb) => {
		cb(null, config.uploadPath);
	},
	filename: (req, file, cb) => {
		cb(null, nanoid() + path.extname(file.originalname));
	}
});

const upload = multer({storage});

const createRouter = () => {
	
	router.post('/', (req, res) => {
		const user = new Users({
			username: req.body.username,
			password: req.body.password
		});
		user.save()
		.then(user => res.send(user))
		.catch(error => res.status(400).send(error))
	});
	
	router.put('/change', [auth, upload.single('image')], async (req, res) => {
		const user = await Users.findOne({_id: req.user._id});
		
		if (req.body.oldPass && req.body.newPass) {
			const isMatch = await user.checkPassword(req.body.oldPass);
			if (!isMatch) return res.status(400).send({error: 'Password is wrong!'});
		}
		
		Users.findOne({_id: user._id}, (err, user) => {
			if (req.file) user.image = req.file.filename;
			if (req.body.oldPass && req.body.newPass) user.password = req.body.newPass;
			if (req.file || (req.body.oldPass && req.body.newPass)) {
				user.save(function (err) {
					if (err) console.error('ERROR!', err);
				});
			} else {
				return res.status(304).send({message: 'Not modified!'});
			}
		}).then(result => res.send(result))
		.catch(error => res.status(400).send(error));
	});
	
	router.post('/sessions', async (req, res) => {
		const user = await Users.findOne({username: req.body.username});
		
		if (!user) {
			return res.status(400).send({error: 'Username not found'});
		}
		
		const isMatch = await user.checkPassword(req.body.password);
		
		if (!isMatch) {
			return res.status(400).send({error: 'Password is wrong!'});
		}
		
		const token = user.generateToken();
		
		return res.send({message: 'User and password correct!', user, token});
	});
	
	router.post('/facebookLogin', async (req, res) => {
		const debugTokenUrl = `https://graph.facebook.com/debug_token?input_token=${req.body.accessToken}&access_token=${config.facebook.facebookAppId}|${config.facebook.appSecret}`
		
		try {
			const response = await request(debugTokenUrl);
			
			const decodedResponse = JSON.parse(response);
			
			if (decodedResponse.data.error) {
				return res.status(401).send({message: 'Facebook token incorrect'});
			}
			
			if (req.body.id !== decodedResponse.data.user_id) {
				return res.status(401).send({message: 'Wrong user ID'});
			}
			
			let user = await Users.findOne({facebookId: req.body.id});
			
			if (!user) {
				user = new Users({
					username: req.body.email,
					password: nanoid(),
					facebookId: req.body.id,
					displayName: req.body.name
				});
				
				await user.save();
			}
			
			let token = user.generateToken();
			
			return res.send({message: 'Login or register successful', user, token});
			
		} catch (error) {
			console.log(error);
			return res.status(401).send({message: 'Facebook token incorrect'});
		}
	});
	
	router.delete('/sessions', async (req, res) => {
		const token = req.get('Token');
		
		const success = {message: 'Logout success!'};
		
		if (!token) return res.send(success);
		
		const user = await Users.findOne({token});
		
		if (!user) return res.send(success);
		
		user.generateToken();
		
		user.save();
		
		return res.send(success);
	});
	
	return router;
};

module.exports = createRouter;