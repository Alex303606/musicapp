const express = require('express');
const TrackHistory = require('../models/TrackHistory');
const auth = require('../middleware/auth');
const moment = require('moment');
const router = express.Router();

const createRouter = () => {
	router.post('/', auth, (req, res) => {
		const track = {};
		track.track = req.body.track;
		track.dateTime = moment().format('Do MMMM  YYYY, h:mm');
		track.user = req.user._id;
		const history = new TrackHistory(track);
		
		history.save()
		.then(result => res.send(result))
		.catch(error => res.status(400).send(error));
	});
	
	router.get('/', auth, (req, res) => {
		const user = req.user._id;
		TrackHistory.find({user: user}).populate({
			path: 'track',
			populate: {
				path: 'album',
				populate: {
					path: 'artist'
				}
			}
		})
		.then(result => {
			if (result) res.send(result);
			else res.sendStatus(404);
		})
		.catch(() => res.sendStatus(500));
	});
	
	return router;
};

module.exports = createRouter;