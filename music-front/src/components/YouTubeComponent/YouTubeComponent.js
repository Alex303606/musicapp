import React, { Fragment } from 'react';
import YouTube from 'react-youtube';
import './YouTubeComponent.css';
import close from '../../assets/images/close.png';
import { connect } from "react-redux";
import { toggleYoutubeVisible } from "../../store/actions/tracks_history";

class YouTubeComponent extends React.Component {
	
	_onReady = (event) => {
		// access to player in all event handlers via event.target
		event.target.playVideo();
	};
	
	closeModal = () => {
		this.props.toggleYoutubeVisible();
	};
	
	render() {
		const opts = {
			height: '390',
			width: '640',
			playerVars: { // https://developers.google.com/youtube/player_parameters
				autoplay: 1
			}
		};
		
		const youtube = (<YouTube
			videoId={this.props.id}
			opts={opts}
			onReady={this._onReady}
		/>);
		
		return (
			<Fragment>
				<div className="wrapper"
				     style={this.props.youTubeModalVisible ? {display: 'block'} : {display: 'none'}}/>
				<div className="youtube_modal"
				     style={this.props.youTubeModalVisible ? {transform: 'translateX(-50%) translateY(-50%)'} : {transform: 'translateX(-50%) translateY(-300%)'}}>
					<span className="close_youtube_modal"
					      onClick={this.closeModal}
					      style={{backgroundImage: 'url(' + close + ')', backgroundSize: 'cover'}}/>
					{this.props.youTubeModalVisible && youtube}
				</div>
			</Fragment>
		);
	}
}

const mapStateToProps = state => ({
	youTubeModalVisible: state.his.youTubeModalVisible
});

const mapDispatchToProps = dispatch => ({
	toggleYoutubeVisible: () => dispatch(toggleYoutubeVisible())
});

export default connect(mapStateToProps,mapDispatchToProps)(YouTubeComponent);