import React, { Component } from 'react';
import { connect } from 'react-redux';
import './AddCocktail.css';
import { FaImage } from "react-icons/lib/fa/index";
import notFound from '../../assets/images/not-found.jpeg';
import { MdSave } from "react-icons/lib/md/index";
import { MdDelete } from "react-icons/lib/md/index";
import { addCocktail } from "../../store/actions/coctails";

class AddCocktail extends Component {
	state = {
		image: '',
		name: '',
		recipe: '',
		preview: null,
		ingredients: [
			{name: '', amount: ''}
		]
	};
	
	inputChangeHandler = event => {
		this.setState({
			[event.target.name]: event.target.value
		});
	};
	
	ingredientChangeHandler = (index, event) => {
		const ingredients = [...this.state.ingredients];
		ingredients[index][event.target.name] = event.target.value;
		this.setState({ingredients});
	};
	
	fileChangeHandler = event => {
		if (event.target.files && event.target.files[0]) {
			let reader = new FileReader();
			reader.onload = (event) => {
				this.setState({preview: event.target.result});
			};
			reader.readAsDataURL(event.target.files[0]);
		}
		this.setState({
			[event.target.name]: event.target.files[0]
		});
	};
	
	submitFormHandler = event => {
		event.preventDefault();
		const formData = new FormData();
		Object.keys(this.state).forEach(key => {
			if(key === 'ingredients') return formData.append('ingredients', JSON.stringify(this.state.ingredients));
			if (key !== 'preview') formData.append(key, this.state[key]);
		});
		this.props.addCocktail(formData);
	};
	
	addIngredientHandler = event => {
		event.preventDefault();
		const ingredients = [...this.state.ingredients];
		ingredients.push({name: '', amount: ''});
		this.setState({ingredients});
	};
	
	removeIngredientHandler = (index, event) => {
		event.preventDefault();
		const ingredients = [...this.state.ingredients];
		if (ingredients.length > 1) ingredients.splice(index, 1);
		this.setState({ingredients});
	};
	
	render() {
		let image = notFound;
		return (
			<div className='container add_cocktail'>
				<h2>Add new cocktail</h2>
				<div className="add_form">
					<div className="add_form_image">
						<img src={this.state.preview || image} alt="avatar"/>
					</div>
					<div className="add_form_info">
						<form onSubmit={this.submitFormHandler}>
							<div className="row">
								<label htmlFor="name">Cocktail name:</label>
								<input required onChange={this.inputChangeHandler}
								       value={this.state.name}
								       id="name" name="name"
								       type="text"/>
							</div>
							
							{this.state.ingredients.map(ingredient => {
								const index = this.state.ingredients.indexOf(ingredient);
								return (
									<div key={index} className="ingredients row">
										<input required onChange={(event) => this.ingredientChangeHandler(index, event)}
										       value={this.state.ingredients[index].name}
										       name="name" type="text"
										       placeholder="Ingredient name"/>
										<input required onChange={(event) => this.ingredientChangeHandler(index, event)}
										       value={this.state.ingredients[index].amount}
										       name="amount" type="text"
										       placeholder="Amount"/>
										<button onClick={(event) => this.removeIngredientHandler(index, event)}
										        className="btn"><MdDelete/>
										</button>
									</div>
								);
							})}
							
							<div className="row">
								<button onClick={this.addIngredientHandler}
								        className="btn">Add ingredient</button>
							</div>
							<div className="row">
								<label htmlFor="recipe">Recipe:</label>
								<textarea required onChange={this.inputChangeHandler}
								          value={this.state.recipe} id="recipe"
								          name="recipe"/>
							</div>
							<div className="row">
								<label className="addImage" htmlFor="image">
									<FaImage/><span>Add image</span>
								</label>
								<input id="image"
								       style={{display: 'none'}}
								       name="image"
								       onChange={this.fileChangeHandler}
								       type="file"/>
							</div>
							<button className="btn save_artist"><MdSave/><span>Save cocktail</span></button>
						</form>
					</div>
				</div>
			</div>
		);
	}
}

const mapDispatchToProps = dispatch => ({
	addCocktail: cocktailData => dispatch(addCocktail(cocktailData))
});

export default connect(null, mapDispatchToProps)(AddCocktail);

