import React, { Component } from 'react';
import { connect } from 'react-redux';
import { FaImage } from "react-icons/lib/fa/index";
import notFound from '../../assets/images/not-found.jpeg';
import './AddArtist.css';
import { MdSave } from "react-icons/lib/md/index";
import { addArtist } from "../../store/actions/artists";
import { toggleModalLogin } from "../../store/actions/users";

class AddArtist extends Component {
	
	
	componentDidMount() {
		if (!this.props.user) {
			this.props.history.push('/');
			this.props.toggleModalLogin();
		}
	}
	
	state = {
		image: '',
		name: '',
		description: '',
		preview: null
	};
	
	inputChangeHandler = event => {
		this.setState({
			[event.target.name]: event.target.value
		});
	};
	
	fileChangeHandler = event => {
		if (event.target.files && event.target.files[0]) {
			let reader = new FileReader();
			reader.onload = (event) => {
				this.setState({preview: event.target.result});
			};
			reader.readAsDataURL(event.target.files[0]);
		}
		this.setState({
			[event.target.name]: event.target.files[0]
		});
	};
	
	submitFormHandler = event => {
		event.preventDefault();
		const formData = new FormData();
		Object.keys(this.state).forEach(key => {
			if (key !== 'preview') formData.append(key, this.state[key]);
		});
		this.props.addArtist(formData);
	};
	
	render() {
		let image = notFound;
		return (
			<div className="container">
				<h1>Add artist</h1>
				<div className="add_form">
					<div className="add_form_image">
						<img src={this.state.preview || image} alt="avatar"/>
					</div>
					<div className="add_form_info">
						<form onSubmit={this.submitFormHandler}>
							<div className="row">
								<label htmlFor="name">Artist name:</label>
								<input onChange={this.inputChangeHandler} value={this.state.name} id="name" name="name"
								       type="text"/>
							</div>
							<div className="row">
								<label htmlFor="descr">Description:</label>
								<textarea onChange={this.inputChangeHandler} value={this.state.description} id="descr"
								          name="description"/>
							</div>
							<div className="row">
								<label className="addImage" htmlFor="image"><FaImage/><span>Add image</span></label>
								<input id="image"
								       style={{display: 'none'}}
								       name="image"
								       onChange={this.fileChangeHandler}
								       type="file"/>
							</div>
							<button className="btn save_artist"><MdSave/><span>Save artist</span></button>
						</form>
					</div>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	user: state.usr.user
});

const mapDispatchToProps = dispatch => ({
	addArtist: artistData => dispatch(addArtist(artistData)),
	toggleModalLogin: () => dispatch(toggleModalLogin())
});

export default connect(mapStateToProps, mapDispatchToProps)(AddArtist);
