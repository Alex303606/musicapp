import {
	DELETE_MESSAGE,
	LAST_MESSAGES,
	LOGGED_IN_USERS,
	NEW_MESSAGE,
	USER_LOGGED_IN,
	USER_LOGGED_OUT
} from "../actions/actionType";

const initialState = {
	messages: [],
	users: [],
	loggedIn: false
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case NEW_MESSAGE:
			return {...state, messages: [...state.messages, action.message]};
		case LAST_MESSAGES:
			return {...state, messages: action.messages};
		case LOGGED_IN_USERS:
			return {...state, users: action.users};
		case USER_LOGGED_IN:
			const newUser = action.user;
			const userIndex = state.users.findIndex(user => user._id === newUser._id);
			if (userIndex === -1) return {...state, users: [...state.users, action.user]};
			else return state;
		case DELETE_MESSAGE:
			const messages = state.messages.filter(message => message._id !== action.id);
			return {...state, messages};
		case USER_LOGGED_OUT:
			const loggedOutUser = action.user;
			const users = [...state.users];
			const index = users.findIndex(user => user._id === loggedOutUser._id);
			users.splice(index, 1);
			return {...state, users: users};
		default:
			return state;
	}
};

export default reducer;
