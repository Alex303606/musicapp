const express = require('express');
const multer = require('multer');
const nanoid = require('nanoid');
const path = require('path');
const Tracks = require('../models/Tracks');
const Albums = require("../models/Albums");
const auth = require('../middleware/auth');
const config = require('../config');

const storage = multer.diskStorage({
	destination: (req, file, cb) => {
		cb(null, config.uploadPath);
	},
	filename: (req, file, cb) => {
		cb(null, nanoid() + path.extname(file.originalname));
	}
});

const upload = multer({storage});

const router = express.Router();

const createRouter = () => {
	
	router.post('/', [auth, upload.fields([
		{name: '', maxCount: 1},
		{album: '', maxCount: 1},
		{duration: '', maxCount: 1},
		{number: '', maxCount: 1},
		{youtube: '', maxCount: 1}
	])], (req, res) => {
		
		const track = new Tracks(req.body);
		track.save()
		.then(result => res.send(result))
		.catch(error => res.status(400).send(error));
	});
	
	router.get('/', (req, res) => {
		const album = req.query.album;
		const artist = req.query.artist;
		if (album) {
			Tracks.find({album: album, public: true})
			.then(result => {
				if (result) {
					const compareNumeric = (a, b) => {
						if (a.number > b.number) return 1;
						if (a.number < b.number) return -1;
					};
					result.sort(compareNumeric);
					res.send(result);
				}
				else res.sendStatus(404);
			})
			.catch(() => res.sendStatus(500));
		} else if (artist) {
			Albums.find({artist: artist, public: true})
			.then(albums => {
				const albumsList = albums.map(album => {
					return album._id;
				});
				Promise.all(albumsList).then(() => {
					Tracks.find({album: albumsList})
					.then(result => {
						if (result) res.send(result);
						else res.sendStatus(404);
					})
					.catch(() => res.sendStatus(500));
				});
			})
			.catch(() => res.sendStatus(500));
		} else {
			Tracks.find({public: true})
			.then(result => {
				if (result) res.send(result);
				else res.sendStatus(404);
			})
			.catch(() => res.sendStatus(500));
		}
	});
	
	return router;
};

module.exports = createRouter;