import { FETCH_ALBUM_INFO_SUCCESS, FETCH_ARTIST_ALBUMS_SUCCESS } from "../actions/actionType";

const initialState = {
	currentArtistAlbums: [],
	album: {}
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case FETCH_ARTIST_ALBUMS_SUCCESS:
			return {...state, currentArtistAlbums: action.albums};
		case FETCH_ALBUM_INFO_SUCCESS:
			return {...state, album: action.album};
		default:
			return state;
	}
};

export default reducer;