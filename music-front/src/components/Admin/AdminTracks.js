import React from 'react';

const AdminTracks = ({tracks, publicTrack, deleteTrack}) => {
	return (
		<div className="admin_panel_list">
			<h2>Tracks</h2>
			<ul>
				{
					tracks.map(track => {
						return <li key={track._id} className="admin_item">
							<span>{track.name}</span>
							{!track.public && <button onClick={() => publicTrack(track._id)}
							                          className="admin_btn admin_public">Public</button>}
							<button onClick={() => deleteTrack(track._id)}
							        className="admin_btn admin_delete">Delete
							</button>
						</li>
					})
				}
			</ul>
		</div>
	);
};

export default AdminTracks;
