import React from 'react';
import { Redirect, Route, Switch } from "react-router-dom";
import Artists from "../../containers/Artists/Artists";
import Profile from "../../containers/Profile/Profile";
import AdminPanel from "../../containers/AdminPanel/AdminPanel";
import Register from "../../containers/Register/Register";
import AlbumPage from "../AlbumPage/AlbumPage";
import TracksHistory from "../../containers/TracksHistory/TracksHistory";
import AddArtist from "../../containers/AddArtist/AddArtist";
import ArtistPage from "../ArtistPage/ArtistPage";
import AddAlbum from "../../containers/AddAlbum/AddAlbum";
import AddTrack from "../../containers/AddTrack/AddTrack";
import Chat from "../../containers/Chat/Chat";
import Cocktails from "../../containers/Cocktails/Cocktails";
import OneCocktailPage from "../OneCocktailPage/OneCocktailPage";

const ProtectRoute = ({isAllowed, ...props}) => (
	isAllowed ? <Route {...props} /> : <Redirect to="/" />
);

const Routes = ({user}) => {
	return (
		<Switch>
			<Route path="/" exact component={Artists}/>
			<Route path="/artists:id" exact component={ArtistPage}/>
			<Route path="/albums:id" exact component={AlbumPage}/>
			<Route path="/register" exact component={Register}/>
			<Route path="/track_history" exact component={TracksHistory}/>
			<Route path="/profile" exact component={Profile}/>
			<Route path="/add_artist" exact component={AddArtist}/>
			<Route path="/add_album" exact component={AddAlbum}/>
			<Route path="/add_track" exact component={AddTrack}/>
			<Route path="/chat" exact component={Chat}/>
			<Route path="/cocktails" exact component={Cocktails}/>
			<Route path="/cocktails/add_cocktail" exact component={Cocktails}/>
			<Route path="/cocktails:id" exact component={OneCocktailPage}/>
			<ProtectRoute isAllowed={user && user.role === 'admin'} path="/admin" component={AdminPanel}/>
		</Switch>
	);
};

export default Routes;
