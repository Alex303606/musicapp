import { FETCH_ARTIST_SUCCESS, FETCH_ARTISTS_SUCCESS } from "../actions/actionType";

const initialState = {
	artists: [],
	currentArtist: {}
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case FETCH_ARTISTS_SUCCESS:
			return {...state, artists: action.artists};
		case FETCH_ARTIST_SUCCESS:
			return {...state, currentArtist: action.artist};
		default:
			return state;
	}
};

export default reducer;