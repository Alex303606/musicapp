import React from 'react';

const AdminArtists = ({artists, publicArtist, deleteArtist}) => {
	return (
		<div className="admin_panel_list">
			<h2>Artists</h2>
			<ul>
				{
					artists.map(artist => {
						return <li key={artist._id} className="admin_item">
							<span>{artist.name}</span>
							{!artist.public && <button onClick={() => publicArtist(artist._id)}
							                           className="admin_btn admin_public">Public</button>}
							<button onClick={() => deleteArtist(artist._id)}
							        className="admin_btn admin_delete">Delete
							</button>
						</li>
					})
				}
			</ul>
		</div>
	);
};

export default AdminArtists;
