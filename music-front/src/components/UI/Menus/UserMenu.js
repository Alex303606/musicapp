import React, { Fragment } from 'react';
import { NavLink } from "react-router-dom";
import { FaHome, FaSignOut, FaUser } from "react-icons/lib/fa/index";
import { MdAlbum, MdAudiotrack, MdSettings } from "react-icons/lib/md/index";

const UserMenu = ({user, avatar, logout}) => {
	return (
		<Fragment>
			<li><NavLink to="/cocktails">Cocktails</NavLink></li>
			<li><NavLink to="/chat"><MdAudiotrack/><span>Chat</span></NavLink></li>
			<li><NavLink to="/track_history">Tracks history</NavLink></li>
			<li className='user_menu'>
				<div><img src={avatar} alt="" className="user_avatar"/>{user.username}</div>
				<ul className="dropdown_menu">
					<li><NavLink to="/profile"><FaHome/><span>My profile</span></NavLink></li>
					<li><NavLink to="/add_artist"><FaUser/><span>Add artist</span></NavLink></li>
					<li><NavLink to="/add_album"><MdAlbum/><span>Add album</span></NavLink></li>
					<li><NavLink to="/add_track"><MdAudiotrack/><span>Add track</span></NavLink></li>
					{user.role === 'admin' && <li><NavLink to="/admin"><MdSettings/><span>Admin panel</span></NavLink></li>}
					<li onClick={logout} className="fake_link"><FaSignOut/><span>Logout</span></li>
				</ul>
			</li>
		</Fragment>
	)
};

export default UserMenu;
