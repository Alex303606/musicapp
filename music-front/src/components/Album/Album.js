import React from 'react';
import './Album.css';
import config from "../../config";
import notFound from '../../assets/images/not-found.jpeg';

const Album = props => {
	let image = `url(${notFound})`;
	
	if (props.image) image = `url(${config.apiUrl}uploads/${props.image})`;
	return (
		<div className="album_item">
			<div className="album_image"
			     onClick={props.click}
			     style={{
				     backgroundImage: image,
				     backgroundRepeat: 'no-repeat',
				     backgroundSize: 'cover',
				     backgroundPosition: 'center'
			     }}/>
			<div className="year"><span>Year of release:</span> {props.year}</div>
		</div>
	);
};

export default Album;
