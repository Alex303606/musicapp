import React, { Component } from 'react';
import { connect } from 'react-redux';
import StarRatings from 'react-star-ratings';
import './OneCocktailPage.css';
import { fetchCocktailById, submitCocktailRating } from "../../store/actions/coctails";
import config from "../../config";
import notFound from '../../assets/images/not-found.jpeg';
import { toggleModalLogin } from "../../store/actions/users";

class OneCocktailPage extends Component {
	
	componentDidMount() {
		if (this.props.user) {
			this.props.fetchCocktailById(this.props.match.params.id);
			
		} else {
			this.props.history.push('/');
			this.props.toggleModalLogin();
		}
	}
	
	componentWillReceiveProps(nextProps) {
		if (nextProps.user) {
			const index = nextProps.currentCocktail.votes.findIndex(vote => vote.rateUser === nextProps.user._id);
			if (index === -1) return this.setState({rating: 0});
			else return this.setState({rating: nextProps.currentCocktail.votes[index].rate});
		}
	}
	
	state = {
		rating: 0
	};
	
	changeRating = newRating => {
		this.setState({rating: newRating});
	};
	
	submitRating = () => {
		this.props.submitCocktailRating(this.props.match.params.id, this.state.rating);
	};
	
	render() {
		let image = `url(${notFound})`;
		
		if (this.props.currentCocktail.image) image = `${config.apiUrl}uploads/${this.props.currentCocktail.image}`;
		
		return (
			<div className="container cocktails_page">
				<div className="cocktails_page__inner">
					<div className="cocktail_image">
						<img src={image} alt=""/>
					</div>
					<div className="cocktail_info">
						<h2>Name: {this.props.currentCocktail.name}</h2>
						<div className="rating">
							<span className="rating_title">Rating:</span>
							<StarRatings
								rating={this.props.currentCocktail.rating}
								starDimension="30px"
								starSpacing="5px"
								starRatedColor="rgba(255,0,0,1)"
							/>
							<div>({this.props.currentCocktail.votes ? this.props.currentCocktail.votes.length : 0} votes)</div>
						</div>
						<h3>Ingredients:</h3>
						<ul className="ingredients">
							{
								this.props.currentCocktail.ingredients && this.props.currentCocktail.ingredients.map(ingredient => {
									return <li key={ingredient.name + ingredient.amount} className="ingredient">
										<span className="ingredient_name">{ingredient.name}: </span>
										<span>{ingredient.amount}</span>
									</li>
								})
							}
						</ul>
						{!this.props.currentCocktail.public &&
							<h1 className="warning">Your cocktail is under consideration by the moderator</h1>
						}
					</div>
				</div>
				<p className="cocktail_recipe">
					<b>Recipe: </b>{this.props.currentCocktail.recipe}
				</p>
				<div className="cocktail_rate">
					<StarRatings
						rating={this.state.rating}
						starHoverColor="yellow"
						starRatedColor="yellow"
						changeRating={this.changeRating}
						numberOfStars={5}
						name='rating'
						starDimension="40px"
						starSpacing="5px"
					/>
					<button onClick={this.submitRating} className="rate_this btn">Rate this</button>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	currentCocktail: state.cocktails.currentCocktail,
	user: state.usr.user
});

const mapDispatchToProps = dispatch => ({
	fetchCocktailById: id => dispatch(fetchCocktailById(id)),
	submitCocktailRating: (id, rate) => dispatch(submitCocktailRating(id, rate)),
	toggleModalLogin: () => dispatch(toggleModalLogin())
});

export default connect(mapStateToProps, mapDispatchToProps)(OneCocktailPage);
