import React from 'react';

const AdminCocktails = ({cocktails, publicCocktail, deleteCocktail}) => {
	return (
		<div className="admin_panel_list">
			<h2>Albums</h2>
			<ul>
				{
					cocktails.map(cocktail => {
						return <li key={cocktail._id} className="admin_item">
							<span>{cocktail.name}</span>
							{!cocktail.public && <button onClick={() => publicCocktail(cocktail._id)}
							                             className="admin_btn admin_public">Public</button>}
							<button onClick={() => deleteCocktail(cocktail._id)}
							        className="admin_btn admin_delete">Delete
							</button>
						</li>
					})
				}
			</ul>
		</div>
	);
};

export default AdminCocktails;
