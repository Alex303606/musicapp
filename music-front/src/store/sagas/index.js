import { takeEvery } from "redux-saga/effects";
import {
	ADD_COCKTAIL, CHANGE_COCKTAIL_RATE,
	DELETE_ADMIN_COCKTAIL,
	FETCH_COCKTAIL,
	FETCH_COCKTAIL_ADMIN, FETCH_COCKTAIL_BY_ID, GOTO_COCKTAIL_PAGE,
	PUBLIC_ADMIN_COCKTAIL
} from "../actions/actionType";
import {
	addCocktailSaga,
	deleteCocktailByAdmin,
	fetchCocktailAdminSaga,
	fetchCocktailSaga,
	goToCocktail,
	goToCocktailById,
	publicCocktailByAdmin,
	changeCocktailRating
} from "./cocktails";

export function* watchCocktails() {
	yield takeEvery(ADD_COCKTAIL, addCocktailSaga);
	yield takeEvery(FETCH_COCKTAIL, fetchCocktailSaga);
	yield takeEvery(FETCH_COCKTAIL_ADMIN, fetchCocktailAdminSaga);
	yield takeEvery(PUBLIC_ADMIN_COCKTAIL, publicCocktailByAdmin);
	yield takeEvery(DELETE_ADMIN_COCKTAIL, deleteCocktailByAdmin);
	yield takeEvery(GOTO_COCKTAIL_PAGE, goToCocktail);
	yield takeEvery(FETCH_COCKTAIL_BY_ID, goToCocktailById);
	yield takeEvery(CHANGE_COCKTAIL_RATE, changeCocktailRating);
}
