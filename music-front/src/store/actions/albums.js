import { FETCH_ALBUM_INFO_SUCCESS, FETCH_ARTIST_ALBUMS_SUCCESS } from "./actionType";
import axios from '../../axios-api';
import { push } from "react-router-redux";

export const fetchArtistAlbumsSuccess = albums => {
	return {type: FETCH_ARTIST_ALBUMS_SUCCESS, albums}
};

export const goToArtistAlbums = id => {
	return dispatch => {
		return axios.get(`/albums?artist=${id}`).then(response => {
			dispatch(fetchArtistAlbumsSuccess(response.data));
		});
	}
};

export const goToAlbum = id => {
	return dispatch => {
		dispatch(push(`/albums${id}`));
	}
};

export const fetchAlbumInfoSuccess = album => {
	return {type: FETCH_ALBUM_INFO_SUCCESS, album};
};

export const goToAlbumInfo = id => {
	return dispatch => {
		return axios.get(`/albums/${id}`).then(response => {
			dispatch(fetchAlbumInfoSuccess(response.data));
		});
	}
};

export const addAlbum = albumData => {
	return (dispatch) => {
		axios.post('/albums', albumData).then(response => {
			dispatch(push(`/albums${response.data._id}`));
		})
	}
};