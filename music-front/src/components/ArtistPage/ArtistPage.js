import React, { Component } from 'react';
import { connect } from 'react-redux';
import { goToArtistInfo } from "../../store/actions/artists";
import notFound from '../../assets/images/not-found.jpeg';
import config from "../../config";
import './ArtistPage.css';
import { goToAlbum, goToArtistAlbums } from "../../store/actions/albums";
import Album from "../Album/Album";

class ArtistPage extends Component {
	
	componentDidMount() {
		this.props.onFetchArtist(this.props.match.params.id);
		this.props.goToArtistAlbums(this.props.match.params.id);
	};
	
	render() {
		let image = notFound;
		if (this.props.artist.image) image = `${config.apiUrl}uploads/${this.props.artist.image}`;
		return (
			<div className="artist__page container">
				<h1>{this.props.artist.name}</h1>
				<div className="artist__page__info">
					<div className="artist__page__image">
						<img src={image} alt="artist_image"/>
					</div>
					<div className="artist__page__descr">{this.props.artist.description}</div>
				</div>
				<div className="artist__page__items">
					{
						this.props.albums.map(album => {
							return <Album
								click={() => this.props.goToAlbum(album._id)}
								key={album._id}
								image={album.image}
								year={album.year}
							/>;
						})
					}
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	artist: state.art.currentArtist,
	albums: state.alb.currentArtistAlbums
});

const mapDispatchToProps = dispatch => ({
	onFetchArtist: id => dispatch(goToArtistInfo(id)),
	goToArtistAlbums: id => dispatch(goToArtistAlbums(id)),
	goToAlbum: id => dispatch(goToAlbum(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(ArtistPage);
