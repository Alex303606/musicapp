import { FETCH_TO_TRACKS_HISTORY_SUCCESS, TOGGLE_YOU_TUBE_VISIBLE } from "../actions/actionType";

const initialState = {
	tracks_history: [],
	youTubeModalVisible: false
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case FETCH_TO_TRACKS_HISTORY_SUCCESS:
			return {...state, tracks_history: action.history};
		case TOGGLE_YOU_TUBE_VISIBLE:
			return {...state, youTubeModalVisible: !state.youTubeModalVisible};
		default:
			return state;
	}
};

export default reducer;