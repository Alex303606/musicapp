import { FETCH_ARTIST_SUCCESS, FETCH_ARTISTS_SUCCESS } from "./actionType";
import axios from '../../axios-api';
import { push } from "react-router-redux";

export const fetchArtistsSuccess = artists => {
	return {type: FETCH_ARTISTS_SUCCESS, artists};
};

export const fetchArtists = () => {
	return dispatch => {
		axios.get('/artists').then(
			response => dispatch(fetchArtistsSuccess(response.data))
		);
	}
};

export const fetchArtistSuccess = artist => {
	return {type: FETCH_ARTIST_SUCCESS, artist};
};

export const goToArtist = id => {
	return dispatch => {
		dispatch(push(`/artists${id}`));
	}
};

export const goToArtistInfo = id => {
	return dispatch => {
		return axios.get(`/artists/${id}`).then(response => {
			dispatch(fetchArtistSuccess(response.data));
		});
	}
};

export const addArtist = artistData => {
	return (dispatch) => {
		axios.post('/artists', artistData).then(response => {
			dispatch(push(`/artists${response.data._id}`));
		})
	}
};