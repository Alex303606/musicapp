import { FETCH_COCKTAILS_SUCCESS, GOTO_COCKTAIL_BY_ID_SUCCESS } from "../actions/actionType";

const initialState = {
	cocktails: [],
	currentCocktail: {}
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case FETCH_COCKTAILS_SUCCESS:
			return {...state, cocktails: action.cocktails};
		case GOTO_COCKTAIL_BY_ID_SUCCESS:
			return {...state, currentCocktail: action.cocktail};
		default:
			return state;
	}
};

export default reducer;