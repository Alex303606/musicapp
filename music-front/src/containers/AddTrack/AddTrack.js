import React, { Component } from 'react';
import { connect } from 'react-redux';
import { MdSave } from "react-icons/lib/md/index";
import Select from 'react-select';
import './AddTrack.css';
import { fetchArtists } from "../../store/actions/artists";
import { goToArtistAlbums } from "../../store/actions/albums";
import { addTrack } from "../../store/actions/tracks";
import { toggleModalLogin } from "../../store/actions/users";

class AddTrack extends Component {
	
	componentDidMount() {
		if(this.props.user) {
			this.props.fetchArtists();
		} else {
			this.props.history.push('/');
			this.props.toggleModalLogin();
		}
	};
	
	state = {
		name: '',
		number: '',
		youtube: '',
		album: '',
		artist: '',
		duration: ''
	};
	
	inputChangeHandler = event => {
		this.setState({
			[event.target.name]: event.target.value
		});
	};
	
	submitFormHandler = event => {
		event.preventDefault();
		const formData = new FormData();
		Object.keys(this.state).forEach(key => {
			if (key === 'album') formData.append(key, this.state[key].value);
			else if (key !== 'artist') formData.append(key, this.state[key]);
		});
		this.props.addTrack(formData);
	};
	
	handleChangeArtist = (artist) => {
		this.setState({artist});
		if (artist) this.props.goToArtistAlbums(artist.value);
	};
	
	handleChangeAlbum = (album) => {
		this.setState({album});
	};
	
	render() {
		let artists = this.props.artists.map(artist => ({value: artist._id, label: artist.name}));
		let albums = this.props.albums.map(album => ({value: album._id, label: album.name}));
		return (
			<div className="container">
				<h1>Add track</h1>
				<div className="add_form_info">
					<form onSubmit={this.submitFormHandler}>
						<div className="row">
							<label>Select artist:</label>
							<Select
								name="artist"
								value={this.state.artist}
								onChange={this.handleChangeArtist}
								options={artists}
							/>
						</div>
						<div className="row">
							<label>Select album:</label>
							<Select
								name="album"
								value={this.state.album}
								onChange={this.handleChangeAlbum}
								options={albums}
							/>
						</div>
						<div className="row">
							<label htmlFor="name">Track name:</label>
							<input onChange={this.inputChangeHandler} value={this.state.name} id="name" name="name"
							       type="text"/>
						</div>
						<div className="row">
							<label htmlFor="number">Track number:</label>
							<input onChange={this.inputChangeHandler} type="number" value={this.state.number}
							       id="number" name="number"/>
						</div>
						<div className="row">
							<label htmlFor="duration">Track duration:</label>
							<input onChange={this.inputChangeHandler} type="number" value={this.state.duration}
							       id="duration" name="duration"/>
						</div>
						<div className="row">
							<label htmlFor="youtube">Track youtube link:</label>
							<input onChange={this.inputChangeHandler} type="text" value={this.state.youtube}
							       id="youtube" name="youtube"/>
						</div>
						<button className="btn"><MdSave/><span>Save track</span></button>
					</form>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	artists: state.art.artists,
	albums: state.alb.currentArtistAlbums,
	user: state.usr.user
});
const mapDispatchToProps = dispatch => ({
	fetchArtists: () => dispatch(fetchArtists()),
	goToArtistAlbums: id => dispatch(goToArtistAlbums(id)),
	addTrack: track => dispatch(addTrack(track)),
	toggleModalLogin: () => dispatch(toggleModalLogin())
});

export default connect(mapStateToProps, mapDispatchToProps)(AddTrack);
