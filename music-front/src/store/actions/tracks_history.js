import axios from "../../axios-api";
import { FETCH_TO_TRACKS_HISTORY_SUCCESS, TOGGLE_YOU_TUBE_VISIBLE } from "./actionType";

export const addTrackToHistory = track => {
	return (dispatch) => {
		axios.post('/track_history', {track: track}).then(() => {
			dispatch(toggleYoutubeVisible());
		});
	}
};

export const getTrackHistory = () => {
	return (dispatch) => {
		axios.get('/track_history').then(response => {
			dispatch(fetchToTracksHistorySuccess(response.data));
		});
	}
};

export const fetchToTracksHistorySuccess = history => {
	return {type: FETCH_TO_TRACKS_HISTORY_SUCCESS, history};
};

export const toggleYoutubeVisible = () => {
	return {type: TOGGLE_YOU_TUBE_VISIBLE};
};