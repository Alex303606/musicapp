import React from 'react';
import PropTypes from 'prop-types';
import notFound from '../../assets/images/not-found.jpeg';
import config from "../../config";
import './OneArtist.css';

const OneArtist = props => {
	
	let image = `url(${notFound})`;
	
	if (props.image) image = `url(${config.apiUrl}uploads/${props.image})`;
	
	return (
		<div className="artist__item" onClick={props.click}>
			<div className="image" style={{
				backgroundImage: image,
				backgroundRepeat: 'no-repeat',
				backgroundSize: 'cover',
				backgroundPosition: 'center'
			}}/>
			<div className="name">{props.name}</div>
		</div>
	);
};

OneArtist.propTypes = {
	image: PropTypes.string,
	name: PropTypes.string.isRequired,
	description: PropTypes.string,
	click: PropTypes.func.isRequired
};

export default OneArtist;
