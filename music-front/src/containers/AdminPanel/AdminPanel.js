import React, { Component } from 'react';
import { connect } from 'react-redux';
import './AdminPanel.css';
import { toggleModalLogin } from "../../store/actions/users";
import { NavLink, Route, Switch } from "react-router-dom";
import AdminArtists from "../../components/Admin/AdminArtists";
import AdminAlbums from "../../components/Admin/AdminAlbums";
import AdminTracks from "../../components/Admin/AdminTracks";
import AdminCocktails from "../../components/Admin/AdminCocktails";
import { MdAlbum, MdAudiotrack } from "react-icons/lib/md/index";
import { FaUser } from "react-icons/lib/fa/index";
import {
	fetchAdminArtists,
	getAdminAllAlbums,
	getTracksAllAlbums,
	publicAdminArtists,
	publicAdminAlbum,
	publicAdminTrack,
	deleteAdminArtists,
	deleteAdminAlbum,
	deleteAdminTrack, fetchCocktailAdmin, publicAdminCocktail, deleteAdminCocktail
} from "../../store/actions/admin";

class AdminPanel extends Component {
	
	componentDidMount() {
		if (!this.props.user) {
			this.props.history.push('/');
			this.props.toggleModalLogin();
		} else if (this.props.user.role === 'admin') {
			this.props.fetchAdminArtists();
			this.props.getAdminAllAlbums();
			this.props.getTracksAllAlbums();
			this.props.fetchCocktailAdmin();
		} else {
			this.props.history.push('/');
		}
	}
	
	render() {
		return (
			<div className="container admin_panel">
				<h1>Admin panel</h1>
				<ul className="admin_menu">
					<li><NavLink to="/admin/artists"><FaUser/><span>Artists</span></NavLink></li>
					<li><NavLink to="/admin/albums"><MdAlbum/><span>Albums</span></NavLink></li>
					<li><NavLink to="/admin/tracks"><MdAudiotrack/><span>Tracks</span></NavLink></li>
					<li><NavLink to="/admin/cocktails"><MdAudiotrack/><span>Cocktails</span></NavLink></li>
				</ul>
				<Switch>
					<Route
						render={() => <p className="title">This is admin panel. Choose menu category.</p>}
						path="/admin" exact/>
					<Route
						render={() => <AdminArtists
							artists={this.props.artists}
							publicArtist={this.props.publicAdminArtists}
							deleteArtist={this.props.deleteAdminArtists}
						/>}
						path="/admin/artists" exact/>
					<Route
						render={() => <AdminAlbums
							albums={this.props.albums}
							publicAlbum={this.props.publicAdminAlbum}
							deleteAlbum={this.props.deleteAdminAlbum}
						/>}
						path="/admin/albums" exact/>
					<Route
						render={() => <AdminTracks
							tracks={this.props.tracks}
							publicTrack={this.props.publicAdminTrack}
							deleteTrack={this.props.deleteAdminTrack}
						/>}
						path="/admin/tracks" exact/>
					<Route
						render={() => <AdminCocktails
							cocktails={this.props.cocktails}
							publicCocktail={this.props.publicAdminCocktail}
							deleteCocktail={this.props.deleteAdminCocktail}
						/>}
						path="/admin/cocktails" exact/>
				</Switch>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	artists: state.admin.artists,
	tracks: state.admin.tracks,
	albums: state.admin.albums,
	user: state.usr.user,
	cocktails: state.admin.cocktails
});

const mapDispatchToProps = dispatch => ({
	fetchAdminArtists: () => dispatch(fetchAdminArtists()),
	toggleModalLogin: () => dispatch(toggleModalLogin()),
	getAdminAllAlbums: () => dispatch(getAdminAllAlbums()),
	getTracksAllAlbums: () => dispatch(getTracksAllAlbums()),
	fetchCocktailAdmin: () => dispatch(fetchCocktailAdmin()),
	publicAdminArtists: id => dispatch(publicAdminArtists(id)),
	publicAdminAlbum: id => dispatch(publicAdminAlbum(id)),
	publicAdminTrack: id => dispatch(publicAdminTrack(id)),
	deleteAdminArtists: id => dispatch(deleteAdminArtists(id)),
	deleteAdminAlbum: id => dispatch(deleteAdminAlbum(id)),
	deleteAdminTrack: id => dispatch(deleteAdminTrack(id)),
	publicAdminCocktail: id => dispatch(publicAdminCocktail(id)),
	deleteAdminCocktail: id => dispatch(deleteAdminCocktail(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AdminPanel);
