const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CocktailsSchema = new Schema({
	user: {
		type: Schema.Types.ObjectId,
		ref: 'Users',
		required: true
	},
	name: {
		type: String,
		required: true,
	},
	image: String,
	recipe: {
		type: String,
		required: true
	},
	votes: {
		type: [{
			rate: {
				type: Number,
				required: true
			},
			rateUser: {
				type: Schema.Types.ObjectId,
				ref: 'Users',
				required: true
			}
		}]
	},
	rating: {
		type: Number,
		default: 0
	},
	public: {
		type: Boolean,
		default: false,
		enum: [true, false]
	},
	ingredients: {
		type: [
			{
				name: {
					type: String,
					required: true
				},
				amount: {
					type: String,
					required: true
				}
			}
		],
		required: true
	}
});

const Cocktails = mongoose.model('Cocktails', CocktailsSchema);

module.exports = Cocktails;