import React, { Component, Fragment } from 'react';
import { NavLink } from "react-router-dom";
import Login from "../../Login/Login";
import { connect } from "react-redux";
import { toggleModalLogin } from "../../../store/actions/users";

class AnonymousMenu extends Component {
	
	render() {
		return (
			<Fragment>
				<Login/>
				<li><NavLink to="/register" exact>Sign Up</NavLink></li>
				<li><a onClick={this.props.toggleModalLogin}>Login</a></li>
			</Fragment>
		)
	}
}

const mapStateToProps = state => ({
	showModalLogin: state.usr.showModalLogin
});

const mapDispatchToProps = dispatch => ({
	toggleModalLogin: () => dispatch(toggleModalLogin())
});

export default connect(mapStateToProps, mapDispatchToProps)(AnonymousMenu);
