import React, { Component } from 'react';
import { connect } from 'react-redux';
import notFound from '../../assets/images/not-found.jpeg';
import { FaImage } from "react-icons/lib/fa/index";
import { MdSave } from "react-icons/lib/md/index";
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import { fetchArtists } from "../../store/actions/artists";
import './AddAlbum.css';
import { addAlbum } from "../../store/actions/albums";
import { toggleModalLogin } from "../../store/actions/users";

class AddAlbum extends Component {
	
	componentDidMount() {
		if (this.props.user) {
			this.props.fetchArtists();
		} else {
			this.props.history.push('/');
			this.props.toggleModalLogin();
		}
	};
	
	state = {
		image: '',
		name: '',
		year: '',
		preview: null,
		artist: ''
	};
	
	inputChangeHandler = event => {
		this.setState({
			[event.target.name]: event.target.value
		});
	};
	
	fileChangeHandler = event => {
		if (event.target.files && event.target.files[0]) {
			let reader = new FileReader();
			reader.onload = (event) => {
				this.setState({preview: event.target.result});
			};
			reader.readAsDataURL(event.target.files[0]);
		}
		this.setState({
			[event.target.name]: event.target.files[0]
		});
	};
	
	submitFormHandler = event => {
		event.preventDefault();
		const formData = new FormData();
		Object.keys(this.state).forEach(key => {
			if (key === 'artist') formData.append(key, this.state[key].value);
			else if (key !== 'preview') formData.append(key, this.state[key]);
		});
		this.props.addAlbum(formData);
	};
	
	handleChange = (artist) => {
		this.setState({artist});
	};
	
	render() {
		let options = this.props.artists.map(artist => ({value: artist._id, label: artist.name}));
		let image = notFound;
		return (
			<div className="container">
				<h1>Add album</h1>
				<div className="add_form">
					<div className="add_form_image">
						<img src={this.state.preview || image} alt="avatar"/>
					</div>
					<div className="add_form_info">
						<form onSubmit={this.submitFormHandler}>
							<div className="row">
								<label htmlFor="name">Album name:</label>
								<input onChange={this.inputChangeHandler} value={this.state.name} id="name" name="name"
								       type="text"/>
							</div>
							<div className="row">
								<label>Select artist:</label>
								<Select
									name="artist"
									value={this.state.artist}
									onChange={this.handleChange}
									options={options}
								/>
							</div>
							<div className="row">
								<label htmlFor="year">Year of release:</label>
								<input onChange={this.inputChangeHandler} type="number" value={this.state.year}
								       id="year" name="year"/>
							</div>
							<div className="row">
								<label className="addImage" htmlFor="image"><FaImage/><span>Add image</span></label>
								<input id="image"
								       style={{display: 'none'}}
								       name="image"
								       onChange={this.fileChangeHandler}
								       type="file"/>
							</div>
							<button className="btn"><MdSave/><span>Save album</span></button>
						</form>
					</div>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	artists: state.art.artists,
	user: state.usr.user
});

const mapDispatchToProps = dispatch => ({
	fetchArtists: () => dispatch(fetchArtists()),
	addAlbum: albumData => dispatch(addAlbum(albumData)),
	toggleModalLogin: () => dispatch(toggleModalLogin())
});

export default connect(mapStateToProps, mapDispatchToProps)(AddAlbum);
