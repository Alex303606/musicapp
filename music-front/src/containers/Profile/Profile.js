import React, { Component } from 'react';
import { connect } from 'react-redux';
import notFound from '../../assets/images/not-found.jpeg';
import { changeUserData, toggleModalLogin } from "../../store/actions/users";
import Preloader from "../../components/UI/Preloader/Preloader";
import './Profile.css';
import { FaImage } from "react-icons/lib/fa/index";
import { MdSave } from "react-icons/lib/md/index";
import config from "../../config";

class Profile extends Component {
	
	componentDidMount() {
		if (!this.props.user) this.props.history.push('/');
		this.props.toggleModalLogin();
	}
	
	state = {
		image: '',
		preview: '',
		oldPass: '',
		newPass: '',
		confirmPass: '',
		errorPass: false
	};
	
	inputChangeHandler = event => {
		this.setState({
			[event.target.name]: event.target.value
		});
	};
	
	fileChangeHandler = event => {
		if (event.target.files && event.target.files[0]) {
			let reader = new FileReader();
			
			reader.onload = (event) => {
				this.setState({preview: event.target.result});
			};
			
			reader.readAsDataURL(event.target.files[0]);
		}
		this.setState({
			[event.target.name]: event.target.files[0]
		});
	};
	
	submitFormHandler = event => {
		event.preventDefault();
		if (this.state.newPass === this.state.confirmPass) {
			const formData = new FormData();
			Object.keys(this.state).forEach(key => {
				if (this.state[key] !== '' && key !== 'preview' && key !== 'confirmPass' && key !== 'errorPass'){
					formData.append(key, this.state[key]);
				}
			});
			this.props.changeUserData(formData);
		} else {
			this.setState({errorPass: true});
		}
	};
	
	render() {
		if (!this.props.user) return <Preloader/>;
		let image = notFound;
		if(this.props.user.image) image = `${config.apiUrl}uploads/${this.props.user.image}`;
		return (
			<div className="profile_page container">
				<div className="user_image">
					<img src={this.state.preview || image} alt="avatar"/>
				</div>
				<div className="user_info">
					<div className="user_name">Username: <span>{this.props.user.username}</span></div>
					<form onSubmit={this.submitFormHandler}>
						<div className="row">
							<label className="addImage" htmlFor="image"><FaImage/><span>Add image</span></label>
							<input id="image"
							       style={{display: 'none'}}
							       name="image"
							       onChange={this.fileChangeHandler}
							       type="file"/>
						</div>
						<div className="change_password">
							<h3 className="title">Change password:</h3>
							<div className="row">
								<label htmlFor="old_pass">Current password:</label>
								<input onChange={this.inputChangeHandler} value={this.state.oldPass} id="old_pass"
								       name="oldPass" type="password"/>
							</div>
							<div className="row">
								<label htmlFor="new_pass">New password:</label>
								<input onChange={this.inputChangeHandler} value={this.state.newPass} id="new_pass"
								       name="newPass" type="password"/>
								{this.state.errorPass && <span className="error">Passwords do not match</span>}
							</div>
							<div className="row">
								<label htmlFor="confirm_pass">Confirm password:</label>
								<input onChange={this.inputChangeHandler} value={this.state.confirmPass}
								       id="confirm_pass" name="confirmPass" type="password"/>
								{this.state.errorPass && <span className="error">Passwords do not match</span>}
							</div>
						</div>
						<button className="btn save_profile"><MdSave/><span>Save profile</span></button>
					</form>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	user: state.usr.user
});
const mapDispatchToProps = dispatch => ({
	toggleModalLogin: () => dispatch(toggleModalLogin()),
	changeUserData: data => dispatch(changeUserData(data))
});


export default connect(mapStateToProps, mapDispatchToProps)(Profile);
