import React, { Component } from 'react';
import { connect } from 'react-redux';
import './Chat.css';
import { wsDeleteMessage, wsOnMessage, wsSendMessage } from "../../store/actions/messages";

class Chat extends Component {
	
	componentDidMount() {
		if (!this.props.user) return this.props.history.push('/');
		this.props.wsOnMessage();
	}
	
	state = {
		messageText: ''
	};
	
	changeMessageTextHandler = event => {
		this.setState({messageText: event.target.value});
	};
	
	sendMessageHandler = event => {
		event.preventDefault();
		this.props.wsSendMessage(this.state.messageText);
		this.setState({messageText: ''});
	};
	
	render() {
		return (
			<div className="Chat container">
				<h1>Chat</h1>
				<div className="chat_wrap">
					<div className="onlineUsers">
						<span className="title">Online users</span>
						<ul className="room">
							{this.props.users.map(user => {
								return <li key={user._id}>
									<p className="user" style={{
										color: "white"
									}}>{user.username}</p>
								</li>
							})}
						</ul>
					</div>
					<div className="chat_room">
						<span className="title">Chat room</span>
						<ul className="room">
							{this.props.messages.map(message => {
								return <li key={message._id}>
									<div>
										{message.user &&
										<span className="user">{message.user.username}</span>
										}
										<p>{message.text}</p>
									</div>
									{this.props.user && this.props.user.role === 'admin' &&
									<button className="delete_message"
									        onClick={() => this.props.wsDeleteMessage(message._id)}>Delete
									</button>
									}
								</li>
							})}
						</ul>
						<form onSubmit={this.sendMessageHandler}>
							<div className="chat_form">
								<input
									required
									onChange={this.changeMessageTextHandler}
									value={this.state.messageText}
									placeholder="Enter message"
									type="text" name="message"/>
								<button className="btn">Send</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	user: state.usr.user,
	messages: state.messages.messages,
	users: state.messages.users
});

const mapDispatchToProps = dispatch => ({
	wsOnMessage: () => dispatch(wsOnMessage()),
	wsSendMessage: message => dispatch(wsSendMessage(message)),
	wsDeleteMessage: id => dispatch(wsDeleteMessage(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
