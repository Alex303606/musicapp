import React from 'react';
import './HistoryItem.css';

const HistoryItem = props => {
	return (
		<li className="history_item">
			<span className="item_artist">{props.artist}</span>
			<span className="item_track">{props.track}</span>
			<span className="item_date">{props.date}</span>
		</li>
	);
};

export default HistoryItem;
