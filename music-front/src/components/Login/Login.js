import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import './Login.css';
import { loginUser, toggleModalLogin } from "../../store/actions/users";
import FacebookLogin from "../FacebookLogin/FacebookLogin";

class LoginModal extends Component {
	
	state = {
		username: '',
		password: ''
	};
	
	inputChangeHandler = event => {
		this.setState({
			[event.target.name]: event.target.value
		});
	};
	
	submitFormHandler = event => {
		event.preventDefault();
		this.props.loginUser(this.state);
	};
	
	render() {
		return (
			<Fragment>
				<div className="wrapper"
				     style={this.props.showModalLogin ? {display: 'block'} : {display: 'none'}}/>
				<div className="modal_login"
				     style={this.props.showModalLogin ? {transform: 'translateX(-50%) translateY(-50%)'} : {transform: 'translateX(-50%) translateY(-300%)'}}>
					<h1>Login user</h1>
					<form onSubmit={this.submitFormHandler}>
						<div className="row">
							<label htmlFor="username">username:</label>
							<input value={this.state.username} onChange={this.inputChangeHandler} id="username"
							       name="username" type="text"/>
						</div>
						<div className="row">
							<label htmlFor="password">password:</label>
							<input value={this.state.password} onChange={this.inputChangeHandler} id="password"
							       name="password" type="password"/>
						</div>
						{this.props.loginError && <span className="error">{this.props.loginError.error}</span>}
						<div className="row button">
							<button><span>Login</span></button>
							<button type="button" onClick={this.props.toggleModalLogin}><span>Cancel</span></button>
						</div>
					</form>
					<form>
						<FacebookLogin/>
					</form>
				</div>
			</Fragment>
		);
	}
}

const mapStateToProps = state => ({
	showModalLogin: state.usr.showModalLogin,
	loginError: state.usr.loginError,
});

const mapDispatchToProps = dispatch => ({
	toggleModalLogin: () => dispatch(toggleModalLogin()),
	loginUser: userData => dispatch(loginUser(userData))
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginModal);
