import {
	DELETE_MESSAGE,
	LAST_MESSAGES,
	LOGGED_IN_USERS,
	NEW_MESSAGE,
	USER_LOGGED_IN,
	USER_LOGGED_OUT
} from "./actionType";
import config from "../../config";

export const wsOnMessage = () => {
	return (dispatch, getState) => {
		let token = getState().usr.token;
		const start = () => {
			this.websocket = new WebSocket(config.websocketServerLocation + `?token=${token}`);
			
			this.websocket.addEventListener('close', () => {
				console.log('Connection lost. Try reconnect!');
				start();
			});
			
			this.websocket.onmessage = (message) => {
				const decodedMessage = JSON.parse(message.data);
				switch (decodedMessage.type) {
					case NEW_MESSAGE:
						return dispatch(newMessage(decodedMessage.message));
					case LAST_MESSAGES:
						return dispatch(lastMessages(decodedMessage.messages));
					case LOGGED_IN_USERS:
						return dispatch(loggedInUsers(decodedMessage.users));
					case USER_LOGGED_IN:
						return dispatch(userLoggedIn(decodedMessage.user));
					case DELETE_MESSAGE:
						return dispatch(deleteMessage(decodedMessage.id));
					case USER_LOGGED_OUT:
						return dispatch(userLoggedOutSuccess(decodedMessage.user))
					default:
						return true;
				}
			};
		};
		start();
	}
};

const userLoggedOutSuccess = user => {
	return {type: USER_LOGGED_OUT, user};
};

const deleteMessage = id => {
	return {type: DELETE_MESSAGE, id};
};

const userLoggedIn = user => {
	return {type: USER_LOGGED_IN, user};
};

const loggedInUsers = users => {
	return {type: LOGGED_IN_USERS, users}
};


const newMessage = message => {
	return {type: NEW_MESSAGE, message};
};

const lastMessages = messages => {
	return {type: LAST_MESSAGES, messages};
};

export const wsSendMessage = messageText => {
	return () => {
		const message = JSON.stringify({
			type: 'CREATE_MESSAGE',
			text: messageText
		});
		this.websocket.send(message);
	}
};

export const wsDeleteMessage = id => {
	return () => {
		const message = JSON.stringify({
			type: 'DELETE_MESSAGE',
			id: id
		});
		this.websocket.send(message);
	}
};

export const userLoggedOut = () => {
	return () => {
		if(this.websocket) {
			const message = JSON.stringify({type: 'LOGOUT_USER'});
			this.websocket.send(message);
		}
	}
};
