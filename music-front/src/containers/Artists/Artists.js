import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchArtists, goToArtist } from "../../store/actions/artists";
import OneArtist from "../../components/OneArtist/OneArtist";
import './Artists.css';

class Artists extends Component {
	
	componentDidMount() {
		this.props.onFetchArtists();
	}
	
	render() {
		return (
			<div className="container artists">
				<h2>Artists list: </h2>
				<div className="artists__items">
					{
						this.props.artists.map(artist => (
							<OneArtist
								click={() => this.props.goToArtistPage(artist._id)}
								key={artist._id}
								name={artist.name}
								image={artist.image}
								description={artist.description}
							/>
						))
					}
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	artists: state.art.artists
});

const mapDispatchToProps= dispatch => ({
	onFetchArtists: () => dispatch(fetchArtists()),
	goToArtistPage: id => dispatch(goToArtist(id))
});

export default connect(mapStateToProps,mapDispatchToProps)(Artists);
