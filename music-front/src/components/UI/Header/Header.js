import React, { Component } from 'react';
import logo from '../../../assets/images/music_logo.svg';
import './Header.css';
import { NavLink } from "react-router-dom";
import AnonymousMenu from "../Menus/AnonymousMenu";
import UserMenu from "../Menus/UserMenu";
import { connect } from "react-redux";
import { logoutUser } from "../../../store/actions/users";
import config from "../../../config";
import user from '../../../assets/images/user.png';

class Header extends Component {
	render() {
		let avatar = user;
		if(this.props.user && this.props.user.image) avatar = `${config.apiUrl}uploads/${this.props.user.image}`;
		
		return (
			<header>
				<div className="container">
					<NavLink to='/' className="header__logo">
						<img src={logo} alt="logo"/>
					</NavLink>
					<ul>
						{this.props.user ? <UserMenu avatar={avatar} logout={this.props.logoutUser} user={this.props.user}/> : <AnonymousMenu/>}
					</ul>
				</div>
			</header>
		);
	}
}

const mapStateToProps = state => ({
	user: state.usr.user
});

const mapDispatchToProps = dispatch => ({
	logoutUser: () => dispatch(logoutUser())
});

export default connect(mapStateToProps, mapDispatchToProps)(Header);
