import axios from '../../axios-api';
import { put } from "redux-saga/effects";
import { push } from "react-router-redux";
import { NotificationManager } from "react-notifications";
import {
	ADD_COCKTAIL_FAILURE,
	FETCH_COCKTAILS_ADMIN_SUCCESS,
	FETCH_COCKTAILS_SUCCESS, GOTO_COCKTAIL_BY_ID_SUCCESS,
} from "../actions/actionType";
import { fetchCocktailById } from "../actions/coctails";

export function* addCocktailSaga(action) {
	try {
		const response = yield axios.post('/cocktails', action.cocktailData);
		yield put(push(`/cocktails${response.data._id}`));
		yield NotificationManager.success('Success', 'Cocktail Added');
	} catch (error) {
		yield put(addCocktailFailure(error.response.data));
		yield NotificationManager.error('Success', 'Error adding cocktail');
	}
}

export const addCocktailFailure = error => {
	return {type: ADD_COCKTAIL_FAILURE, error};
};

export function* fetchCocktailSaga() {
	try {
		const response = yield axios.get('/cocktails');
		yield put(fetchCocktailsSuccess(response.data));
	} catch (error) {
		yield NotificationManager.error('Success', 'Error fetching cocktail');
	}
}

export const fetchCocktailsSuccess = cocktails => {
	return {type: FETCH_COCKTAILS_SUCCESS, cocktails};
};

export function* fetchCocktailAdminSaga() {
	try {
		const response = yield axios.get('/admin/cocktails');
		yield put(fetchCocktailsAdminSuccess(response.data));
	} catch (error) {
		yield NotificationManager.error('Error', 'Error fetching cocktail');
	}
}

const fetchCocktailsAdminSuccess = cocktails => {
	return {type: FETCH_COCKTAILS_ADMIN_SUCCESS, cocktails};
};

export function* deleteCocktailByAdmin(action) {
	try {
		yield axios.delete(`/admin/cocktails/${action.id}`);
		yield* fetchCocktailAdminSaga();
	} catch (error) {
		yield NotificationManager.error('Error', 'Error delete cocktail');
	}
}

export function* publicCocktailByAdmin(action) {
	try {
		yield axios.post(`/admin/cocktails`, {id: action.id});
		yield* fetchCocktailAdminSaga();
	} catch (error) {
		yield NotificationManager.error('Error', 'Error public cocktail');
	}
}

export function* goToCocktail(action) {
	try {
		yield put(push(`/cocktails${action.id}`));
	} catch (error) {
		yield NotificationManager.error('Error', 'Error go cocktail page');
	}
}

export function* goToCocktailById(action) {
	try {
		const response = yield axios.get(`/cocktails/${action.id}`);
		yield put(goToCocktailByIdSuccess(response.data));
	} catch (error) {
		yield NotificationManager.error('Error', 'Error info cocktail');
	}
}

const goToCocktailByIdSuccess = cocktail => {
	return {type: GOTO_COCKTAIL_BY_ID_SUCCESS, cocktail};
};

export function* changeCocktailRating(action) {
	try {
		yield axios.post(`/cocktails/rate`, {id: action.id, rate: action.rate});
		yield put(fetchCocktailById(action.id));
	} catch (error) {
		yield NotificationManager.error('Error', 'Error rate cocktail');
	}
}