import axios from "../../axios-api";
import {
	DELETE_ADMIN_COCKTAIL,
	FETCH_ADMIN_ALBUMS_SUCCESS,
	FETCH_ADMIN_ARTIST_SUCCESS,
	FETCH_ADMIN_TRACKS_SUCCESS,
	FETCH_COCKTAIL_ADMIN, PUBLIC_ADMIN_COCKTAIL
} from "./actionType";

export const fetchAdminArtists = () => {
	return (dispatch) => {
		axios.get('admin/artists').then(
			response => dispatch(fetchAdminArtistSuccess(response.data))
		);
	}
};

export const publicAdminArtists = id => {
	return (dispatch) => {
		axios.post(`admin/artists`, {id: id}).then(
			() => dispatch(fetchAdminArtists())
		);
	}
};

export const deleteAdminArtists = id => {
	return (dispatch) => {
		axios.delete(`admin/artists/${id}`).then(
			() => dispatch(fetchAdminArtists())
		);
	}
};

export const publicAdminAlbum = id => {
	return (dispatch) => {
		axios.post(`admin/albums`, {id: id}).then(
			() => dispatch(getAdminAllAlbums())
		);
	}
};

export const deleteAdminAlbum = id => {
	return (dispatch) => {
		axios.delete(`admin/albums/${id}`).then(
			() => dispatch(getAdminAllAlbums())
		);
	}
};

export const publicAdminTrack = id => {
	return (dispatch) => {
		axios.post(`admin/tracks`, {id: id}).then(
			() => dispatch(getTracksAllAlbums())
		);
	}
};

export const deleteAdminTrack = id => {
	return (dispatch) => {
		axios.delete(`admin/tracks/${id}`).then(
			() => dispatch(getTracksAllAlbums())
		);
	}
};

const fetchAdminArtistSuccess = artists => {
	return {type: FETCH_ADMIN_ARTIST_SUCCESS, artists};
};

export const getAdminAllAlbums = () => {
	return (dispatch) => {
		axios.get('admin/albums').then(response => {
			dispatch(fetchAdminAlbumsSuccess(response.data));
		})
	}
};

const fetchAdminAlbumsSuccess = albums => {
	return {type: FETCH_ADMIN_ALBUMS_SUCCESS, albums};
};

export const getTracksAllAlbums = () => {
	return (dispatch) => {
		axios.get('admin/tracks').then(response => {
			dispatch(fetchAdminTracksSuccess(response.data));
		})
	}
};

const fetchAdminTracksSuccess = tracks => {
	return {type: FETCH_ADMIN_TRACKS_SUCCESS, tracks};
};

export const fetchCocktailAdmin = () => {
	return {type: FETCH_COCKTAIL_ADMIN};
};

export const publicAdminCocktail = id => {
	return {type: PUBLIC_ADMIN_COCKTAIL, id};
};

export const deleteAdminCocktail = id => {
	return {type: DELETE_ADMIN_COCKTAIL, id};
};