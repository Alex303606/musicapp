import React from 'react';

const AdminAlbums = ({albums, publicAlbum, deleteAlbum}) => {
	return (
		<div className="admin_panel_list">
			<h2>Albums</h2>
			<ul>
				{
					albums.map(album => {
						return <li key={album._id} className="admin_item">
							<span>{album.name}</span>
							{!album.public && <button onClick={() => publicAlbum(album._id)}
							                          className="admin_btn admin_public">Public</button>}
							<button onClick={() => deleteAlbum(album._id)}
							        className="admin_btn admin_delete">Delete
							</button>
						</li>
					})
				}
			</ul>
		</div>
	);
};

export default AdminAlbums;
