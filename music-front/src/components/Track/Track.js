import React from 'react';
import './Track.css';
import buttonPlay from '../../assets/images/play.png';

const Track = props => {
	return (
		<li className="track__item">
			<span className="track_number">{props.number}</span>
			<button onClick={props.addToHistory} style={{
				background: `url(${buttonPlay})`,
				backgroundRepeat: 'no-repeat',
				backgroundPosition: 'center',
				backgroundSize: 'cover'
			}} className="play__track"/>
			<span className="track_name">{props.name}</span>
			<span className="track_duration">{props.duration}</span>
		</li>
	);
};

export default Track;
