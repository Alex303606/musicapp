const express = require('express');
const jwt = require('jsonwebtoken');
const config = require('../config');

const Messages = require('./../models/Messages');
const Users = require('./../models/Users');

const clients = {};
const onlineClients = [];

const broadcast = message => {
	Object.values(clients).forEach(client => {
		client.connection.send(JSON.stringify(message));
	});
};

const sendLastMessages = async ws => {
	const lastMessages = await Messages.find().sort({dateTime: -1}).limit(30).populate('user');
	ws.send(JSON.stringify({
		type: 'LAST_MESSAGES',
		messages: lastMessages.reverse()
	}))
};

const createMessage = async (text, user) => {
	const message = new Messages({text, user: user});
	await message.save();
	broadcast({type: 'NEW_MESSAGE', message});
};

const sendLoggedInUsers = ws => {
	const users = Object.values(clients).map(client => client.user);
	ws.send(JSON.stringify({type: 'LOGGED_IN_USERS', users}));
};

const deleteMessage = async (id) => {
	await Messages.findByIdAndDelete(id);
	broadcast({type: 'DELETE_MESSAGE', id});
};

const sendLoggedOutUsers = (ws, id, user) => {
	delete clients[id];
	const index = onlineClients.findIndex(user => user._id === id);
	onlineClients.splice(index, 1);
	console.log('Client disconnected.');
	console.log('Number of active connections: ', Object.values(clients).length);
	broadcast({type: 'USER_LOGGED_OUT', user});
};

const configureConnection = (ws, req) => {
	const id = req.get('sec-websocket-key');
	const user = req.user;
	
	let userAlreadyExist = false;
	
	onlineClients.forEach(client => {user._id.equals(client._id) ? userAlreadyExist = true : userAlreadyExist = false;});
	
	if (!userAlreadyExist) {
		onlineClients.push(user._id);
		clients[id] = {connection: ws, user};
		broadcast({type: 'USER_LOGGED_IN', user});
	}
	
	sendLoggedInUsers(ws);
	sendLastMessages(ws);
	
	console.log('Client connected. Username: ' + user.username);
	console.log('Number of active connections: ', Object.values(clients).length);
	
	ws.on('message', (msg) => {
		
		let decodedMessage;
		
		try {
			decodedMessage = JSON.parse(msg);
		} catch (e) {
			return ws.send(JSON.stringify({
				type: 'ERROR',
				message: 'Message is not JSON'
			}));
		}
		
		switch (decodedMessage.type) {
			case 'CREATE_MESSAGE':
				createMessage(decodedMessage.text, user);
				break;
			case 'DELETE_MESSAGE':
				if (user.role !== 'admin') {
					ws.send(JSON.stringify({
						type: 'ERROR',
						message: 'Unauthorized'
					}))
				} else {
					deleteMessage(decodedMessage.id);
				}
				break;
			case 'LOGOUT_USER':
				sendLoggedOutUsers(ws, id, user);
				break;
			default:
				return ws.send(JSON.stringify({
					type: 'ERROR',
					message: 'Unknown message type'
				}));
		}
	});
	
	ws.on('close', () => {
		sendLoggedOutUsers(ws, id, user);
	});
};

const createRouter = () => {
	const router = express.Router();
	
	router.ws('/', async (ws, req) => {
		const token = req.query.token;
		if (!token) {
			ws.send(JSON.stringify({type: 'ERROR', message: 'No token present!'}));
			return ws.close();
		}
		
		let tokenData;
		
		try {
			tokenData = jwt.verify(token, config.jwt.secret);
		} catch (e) {
			console.log(e);
			ws.send(JSON.stringify({type: 'ERROR', message: 'Token incorrect!'}));
			return ws.close();
		}
		
		const user = await Users.findById(tokenData.id);
		
		if (!user) {
			ws.send(JSON.stringify({type: 'ERROR', message: 'Not authenticated!'}));
			return ws.close();
		}
		
		req.user = user;
		
		configureConnection(ws, req);
		
	});
	
	return router;
};

module.exports = createRouter;
