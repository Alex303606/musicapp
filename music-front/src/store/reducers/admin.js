import {
	FETCH_ADMIN_ALBUMS_SUCCESS,
	FETCH_ADMIN_ARTIST_SUCCESS,
	FETCH_ADMIN_TRACKS_SUCCESS, FETCH_COCKTAILS_ADMIN_SUCCESS
} from "../actions/actionType";

const initialState = {
	artists: [],
	tracks: [],
	albums: [],
	cocktails: []
};

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case FETCH_ADMIN_ARTIST_SUCCESS:
			return {...state, artists: action.artists};
		case FETCH_ADMIN_ALBUMS_SUCCESS:
			return {...state, albums: action.albums};
		case FETCH_ADMIN_TRACKS_SUCCESS:
			return {...state, tracks: action.tracks};
		case FETCH_COCKTAILS_ADMIN_SUCCESS:
			return {...state, cocktails: action.cocktails};
		default:
			return state;
	}
};

export default reducer;