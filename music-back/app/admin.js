const express = require('express');
const Albums = require('../models/Albums');
const Artists = require('../models/Artists');
const Tracks = require('../models/Tracks');
const Cocktails = require('../models/Cocktails');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');

const router = express.Router();

const createRouter = () => {
	
	router.get('/albums', [auth, permit('admin')], (req, res) => {
		Albums.find()
		.then(result => {
			if (result) res.send(result);
			else res.sendStatus(404);
		})
		.catch(() => res.sendStatus(500));
	});
	
	router.get('/artists', [auth, permit('admin')], (req, res) => {
		Artists.find()
		.then(result => {
			if (result) res.send(result);
			else res.sendStatus(404);
		})
		.catch(() => res.sendStatus(500));
	});
	
	router.get('/tracks', [auth, permit('admin')], (req, res) => {
		Tracks.find()
		.then(result => {
			if (result) res.send(result);
			else res.sendStatus(404);
		})
		.catch(() => res.sendStatus(500));
	});
	
	router.post('/artists',[auth, permit('admin')],(req, res) => {
		Artists.findOne({_id: req.body.id},(err, artist) => {
			artist.public = true;
			artist.save(function (err) {
				if (err) console.error('ERROR!', err);
			});
		}).then(result => {
			if (result) res.send(result);
			else res.sendStatus(404);
		})
		.catch(() => res.sendStatus(500));
	});
	
	router.post('/albums',[auth, permit('admin')],(req, res) => {
		Albums.findOne({_id: req.body.id},(err, album) => {
			album.public = true;
			album.save(function (err) {
				if (err) console.error('ERROR!', err);
			});
		}).then(result => {
			if (result) res.send(result);
			else res.sendStatus(404);
		})
		.catch(() => res.sendStatus(500));
	});
	
	router.post('/tracks',[auth, permit('admin')],(req, res) => {
		Tracks.findOne({_id: req.body.id},(err, track) => {
			track.public = true;
			track.save(function (err) {
				if (err) console.error('ERROR!', err);
			});
		}).then(result => {
			if (result) res.send(result);
			else res.sendStatus(404);
		})
		.catch(() => res.sendStatus(500));
	});
	
	router.delete('/artists/:id',[auth, permit('admin')],(req, res) => {
		Artists.deleteOne({_id: req.params.id})
		.then(result => {
			if (result) res.send(result);
			else res.sendStatus(404);
		})
		.catch(() => res.sendStatus(500));
	});
	
	router.delete('/albums/:id',[auth, permit('admin')],(req, res) => {
		Albums.deleteOne({_id: req.params.id})
		.then(result => {
			if (result) res.send(result);
			else res.sendStatus(404);
		})
		.catch(() => res.sendStatus(500));
	});
	
	router.delete('/tracks/:id',[auth, permit('admin')],(req, res) => {
		Tracks.deleteOne({_id: req.params.id})
		.then(result => {
			if (result) res.send(result);
			else res.sendStatus(404);
		})
		.catch(() => res.sendStatus(500));
	});
	
	router.get('/cocktails',[auth, permit('admin')] ,(req, res) => {
		Cocktails.find()
		.then(result => {
			if (result) res.send(result);
			else res.sendStatus(404);
		})
		.catch(() => res.sendStatus(500));
	});
	
	router.post('/cocktails',[auth, permit('admin')],(req, res) => {
		Cocktails.findOne({_id: req.body.id},(err, cocktail) => {
			cocktail.public = true;
			cocktail.save(function (err) {
				if (err) console.error('ERROR!', err);
			});
		}).then(result => {
			if (result) res.send(result);
			else res.sendStatus(404);
		})
		.catch(() => res.sendStatus(500));
	});
	
	router.delete('/cocktails/:id',[auth, permit('admin')],(req, res) => {
		Cocktails.deleteOne({_id: req.params.id})
		.then(result => {
			if (result) res.send(result);
			else res.sendStatus(404);
		})
		.catch(() => res.sendStatus(500));
	});
	
	return router;
};

module.exports = createRouter;
