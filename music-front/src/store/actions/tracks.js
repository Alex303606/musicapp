import axios from '../../axios-api';
import { FETCH_ALBUM_TRACKS_SUCCESS } from "./actionType";
import { push } from "react-router-redux";

export const fetchAlbumTracksSuccess = tracks => {
	return {type: FETCH_ALBUM_TRACKS_SUCCESS, tracks};
};

export const goToAlbumTracks = id => {
	return dispatch => {
		return axios.get(`/tracks?album=${id}`).then(response => {
			dispatch(fetchAlbumTracksSuccess(response.data));
		});
	}
};

export const addTrack = track => {
	return dispatch => {
		axios.post('/tracks', track).then(response => {
			dispatch(push(`/albums${response.data.album}`));
		})
	}
};