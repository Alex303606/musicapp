const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TracksSchema = new Schema({
	name: {
		type: String,
		required: true,
		unique: true
	},
	number: {
		type: Number,
		required: true
	},
	album: {
		type: Schema.Types.ObjectId,
		ref: 'Albums',
		required: true
	},
	youtube: {
		type: String,
		required: true
	},
	duration: String,
	public: {
		type: Boolean,
		default: false,
		enum: [true, false]
	}
});

const Tracks = mongoose.model('Tracks', TracksSchema);

module.exports = Tracks;