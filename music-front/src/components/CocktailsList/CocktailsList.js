import React, { Component } from 'react';
import { connect } from 'react-redux';
import './CocktailsList.css';
import { fetchCocktail, goToCocktailPage } from "../../store/actions/coctails";
import Cocktail from "../Cocktail/Cocktail";

class CocktailsList extends Component {
	
	componentDidMount() {
		this.props.fetchCocktail()
	}
	
	render() {
		return (
			<div className="cocktails_items">
				{
					this.props.cocktails.map(cocktail => (
						<Cocktail key={cocktail._id}
					          cocktailImage={cocktail.image}
					          name={cocktail.name}
					          click={() => this.props.goToCocktailPage(cocktail._id)}
						/>
					))
				}
			</div>
		);
	}
}

const mapStateToProps = state => ({
	cocktails: state.cocktails.cocktails
});
const mapDispatchToProps = dispatch => ({
	fetchCocktail: () => dispatch(fetchCocktail()),
	goToCocktailPage: id => dispatch(goToCocktailPage(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(CocktailsList);
