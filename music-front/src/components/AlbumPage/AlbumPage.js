import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { goToAlbumInfo } from "../../store/actions/albums";
import config from "../../config";
import notFound from '../../assets/images/not-found.jpeg';
import './AlbumPage.css';
import { goToArtist } from "../../store/actions/artists";
import { addTrackToHistory } from "../../store/actions/tracks_history";
import Track from "../Track/Track";
import { goToAlbumTracks } from "../../store/actions/tracks";
import { toggleModalLogin } from "../../store/actions/users";
import YouTubeComponent from "../YouTubeComponent/YouTubeComponent";

class AlbumPage extends Component {
	
	state = {
		currentVideoId: null
	};
	
	componentDidMount() {
		this.props.goToAlbumInfo(this.props.match.params.id);
		this.props.goToAlbumTracks(this.props.match.params.id);
	}
	
	playVideo = (id,videoId) => {
		this.props.addTrackToHistory(id);
		this.setState({currentVideoId: videoId});
	};
	
	render() {
		let image = notFound;
		let photo = notFound;
		if (this.props.album.image) image = `${config.apiUrl}uploads/${this.props.album.image}`;
		if (this.props.album.artist && this.props.album.artist.image) photo = `${config.apiUrl}uploads/${this.props.album.artist.image}`;
		return (
			<Fragment>
				<div className="container album__page">
					<div className="album__page__image">
						<img src={image} alt="album_image"/>
					</div>
					<div className="album__info">
						<h1>{this.props.album.name}</h1>
						<h2><span>Year of release: </span>{this.props.album.year}</h2>
						<div className="album__artist">
							<h2>Artist: </h2>
							<div className="album__artist__descr">
								{this.props.album.artist && <img  onClick={() => this.props.goToArtistPage(this.props.album.artist._id)} src={photo} alt="artist_image"/>}
								<h3 onClick={() => this.props.goToArtistPage(this.props.album.artist._id)} >{this.props.album.artist && this.props.album.artist.name}</h3>
							</div>
						</div>
					</div>
				</div>
				<YouTubeComponent id={this.state.currentVideoId}/>
				<div className="tracks__list container">
					<h2>Tracks: </h2>
					<ul>
						{
							this.props.tracks.map(track => {
								return <Track
									addToHistory={this.props.user ? () => this.playVideo(track._id, track.youtube) : this.props.toggleModalLogin}
									key={track._id}
									name={track.name}
									duration={track.duration}
									number={track.number}
								/>
							})
						}
					</ul>
				</div>
			</Fragment>
		);
	}
}

const mapStateToProps = state => (
	{
		album: state.alb.album,
		tracks: state.trc.currentAlbumTracks,
		user: state.usr.user
	}
);


const mapDispatchToProps = dispatch => (
	{
		goToAlbumInfo: id => dispatch(goToAlbumInfo(id)),
		goToArtistPage: id => dispatch(goToArtist(id)),
		goToAlbumTracks: id => dispatch(goToAlbumTracks(id)),
		addTrackToHistory: id => dispatch(addTrackToHistory(id)),
		toggleModalLogin: () => dispatch(toggleModalLogin())
	}
);

export default connect(mapStateToProps,mapDispatchToProps)(AlbumPage);
